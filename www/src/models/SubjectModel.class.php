<?php

namespace Model;

class SubjectModel {
    private $db;

    public function __construct($db) {
        $this->db = $db;
    }

    public function isExists($subject) {
        $sql = 'SELECT COUNT(*)
                FROM `subject`
                WHERE `subject`.`id` = :subject_id';
        $query = $this->db->prepare($sql);
        $query->execute([
            ':subject_id' => $lessonId
        ]);
        return $query->fetch(\PDO::FETCH_ASSOC) != 0;
    }

    public function getAllSubjects() {
        $sql = 'SELECT `subject`.*
                FROM `subject`
                WHERE EXISTS (
                    SELECT `lesson`.`id`
                    FROM `lesson`
                    WHERE `lesson`.`subject_id` = `subject`.`id`)'; // fix
        $query = $this->db->prepare($sql);
        $query->execute([]);
        return $query->fetchAll(\PDO::FETCH_ASSOC);
    }
}
