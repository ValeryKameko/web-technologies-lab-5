<?php

namespace Model;

class MarkModel {
    private $db;

    public function __construct($db) {
        $this->db = $db;
    }

    public function getMarkValue($lessonId, $studentId) {
        $sql = 'SELECT `mark`.`value`
                FROM `mark`
                WHERE 
                    `mark`.`lesson_id` = :lesson_id AND
                    `mark`.`student_id` = :student_id';
        $query = $this->db->prepare($sql);
        $query->execute([
            ':lesson_id' => $lessonId,
            ':student_id' => $studentId,
        ]);
        $entry = $query->fetch(PDO::FETCH_ASSOC);
        if (!$entry) {
            return null;
        } else {
            return $entry['value'];
        }
    }

    public function setMarkValue($lessonId, $studentId, $value) {
        $sql = 'REPLACE INTO `mark` (`lesson_id`, `student_id`, `value`) 
                VALUES (:lesson_id, :student_id, :mark)';
        $query = $this->db->prepare($sql);
        return $query->execute([
            ':lesson_id' => $lessonId,
            ':student_id' => $studentId,
            ':mark' => $value
        ]);
    }


    public function clearMarkValue($lessonId, $studentId) {
        $sql = 'DELETE FROM `mark`
                WHERE
                    `mark`.`lesson_id` = :lesson_id AND
                    `mark`.`student_id` = :student_id';
        $query = $this->db->prepare($sql);
        return $query->execute([
            ':lesson_id' => $lessonId,
            ':student_id' => $studentId,
        ]);
    }
}
