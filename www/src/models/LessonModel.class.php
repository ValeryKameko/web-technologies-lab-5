<?php

namespace Model;

class LessonModel {
    private $db;

    public function __construct($db) {
        $this->db = $db;
    }

    public function isExists($lessonId) {
        $sql = 'SELECT COUNT(*)
                FROM `lesson`
                WHERE `lesson`.`id` = :lesson_id';
        $query = $this->db->prepare($sql);
        $query->execute([
            ':lesson_id' => $lessonId
        ]);
        return $query->fetch(\PDO::FETCH_ASSOC) != 0;
    }

    public function getAllLessons() {
        $sql = 'SELECT `lesson`.*, `subject`.`name` AS `subject_name`
                FROM `lesson`
                INNER JOIN `subject` ON `subject`.`id` = `lesson`.`subject_id`
                ORDER BY `lesson`.`date` ASC';
        $query = $this->db->prepare($sql);
        $query->execute();
        return $query->fetchAll(\PDO::FETCH_ASSOC);
    }
}
