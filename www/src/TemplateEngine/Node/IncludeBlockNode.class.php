<?php

namespace TemplateEngine\Node;

use TemplateEngine\Compiler;
use TemplateEngine\Error\UnimlementedError;

class IncludeBlockNode extends Node
{
    public function __construct($includeExpressionNode, $line)
    {
        parent::__construct([ 'include_expression_node' => $includeExpressionNode ], [], $line, 'include');
    }

    public function compile(Compiler $compiler)
    {
        $compiler->write('$this->displayTemplate(');
        $this->nodes['include_expression_node']->compile($compiler);
        $compiler->writeLine(', $context);');
    }
}