<?php

namespace TemplateEngine\Node;

use TemplateEngine\Compiler;
use TemplateEngine\Error\UnimlementedError;

class PrintNode extends Node
{
    public function __construct($expression)
    {
        parent::__construct(['expression' => $expression], [], NULL, 'print');
    }

    public function compile(Compiler $compiler)
    {
        $compiler->write('echo ');
        $this->nodes['expression']->compile($compiler);
        $compiler->write(';');
        $compiler->endLine();
    }
}