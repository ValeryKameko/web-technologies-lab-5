<?php

namespace TemplateEngine\Node;

use TemplateEngine\Compiler;
use TemplateEngine\Error\UnimlementedError;

class TextNode extends Node
{
    public function __construct($data, $line)
    {
        parent::__construct([], ['data' => $data], $line, 'text');
    }

    public function compile(Compiler $compiler)
    {
        $compiler->write('echo \'');
        $compiler->write(preg_replace('/([\\\'])/', '\\\\$1', $this->attributes['data']));
        $compiler->write('\';');
        $compiler->endLine();
    }
}