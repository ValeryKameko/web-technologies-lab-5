<?php

namespace TemplateEngine\Node;

use TemplateEngine\Compiler;
use TemplateEngine\Error\UnimlementedError;

class ForNode extends Node
{
    public function __construct($forIterator, $forExpression, $forBody, $line)
    {
        $nodes = [
            'for_iterator' => $forIterator,
            'for_expression' => $forExpression,
            'for_body' => $forBody,
        ];
        parent::__construct($nodes, [], $line, 'for');
    }

    public function compile(Compiler $compiler)
    {
        $compiler->writeContextSaving();

        $compiler->write('$context[\'__seq\'] = ');
        $this->nodes['for_expression']->compile($compiler);
        $compiler->writeLine(';');
        
        $compiler->writeLine('$context[\'index\'] = 0;');

        $compiler->writeLine('foreach ($context[\'__seq\'] as $context[\'__key\'] => $context[\'__value\']) {');
        $compiler->indent();

        $this->nodes['for_iterator']->compileNoCheck($compiler);
        $compiler->writeLine(' = $context[\'__value\'];');

        $this->nodes['for_body']->compile($compiler);

        $compiler->writeLine('$context[\'index\'] += 1;');

        $compiler->outdent();
        $compiler->writeLine('}');

        $compiler->writeContextRestore();
    }
}