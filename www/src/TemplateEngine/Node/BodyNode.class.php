<?php

namespace TemplateEngine\Node;

use TemplateEngine\Compiler;
use TemplateEngine\Error\UnimlementedError;

class BodyNode extends Node
{
    public function __construct($nodes, $line)
    {
        parent::__construct($nodes, [], $line, 'body');
    }
}