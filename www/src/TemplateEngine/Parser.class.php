<?php

namespace TemplateEngine;

use TemplateEngine\Node\Node;
use TemplateEngine\Node\TextNode;
use TemplateEngine\Node\BodyNode;
use TemplateEngine\Node\TemplateNode;
use TemplateEngine\Node\PrintNode;
use TemplateEngine\Error\SyntaxError;


class Parser 
{
    private $env;
    private $expressionParserClass;
    private $expressionParser;
    private $blockParsers;
    private $stream;

    public function __construct(Environment $env)
    {
        $this->blockParsers = $env->getOption('block_parsers');
        $this->expressionParserClass = $env->getOption('expression_parser');
        $this->env = $env;
    }

    public function parse(TokenStream $tokenStream) : Node
    {
        $expressionParserClass = $this->expressionParserClass;
        $this->expressionParser = new $expressionParserClass($this->env, $this);
        $this->stream = $tokenStream;
        $node = $this->subparse();
        $bodyNode = new BodyNode($node->getNodes(), $node->getLine());
        $templateNode = new TemplateNode($bodyNode);
        return $templateNode;
    }
    
    public function subparse($hardFail = true) : Node
    {
        $line = $this->getLine();
        $localNodes = [];
        
        while (!$this->isEOF()) {
            switch ($this->currentToken()->getType()) {
                case Token::TEXT_TYPE:
                    $token = $this->next();
                    $node = new TextNode($token->getValue(), $token->getLine());
                    array_push($localNodes, $node);
                    break;
                
                case Token::EXPRESSION_START_TYPE:
                    $token = $this->next();
                    $expressionNode = $this->parseExpression();
                    $node = new PrintNode($expressionNode);
                    if (is_null($node))
                        throw new SyntaxError(
                            'Expected expression', 
                            $this->getLine(),
                            $this->getTokenStream()->getSource());
                
                    array_push($localNodes, $node);
                    $this->expect(Token::EXPRESSION_END_TYPE);
                    break;

                case Token::BLOCK_START_TYPE:
                    $token = $this->next();
                    $token = $this->currentToken();

                    if (isset($this->blockParsers[$token->getValue()])) {
                        $blockParserClass = $this->blockParsers[$token->getValue()];
                        $blockParser = new $blockParserClass();
                        $node = $blockParser->subparse($this);
                        $this->expect(Token::BLOCK_END_TYPE);
                        array_push($localNodes, $node);
                    }
                    elseif ($hardFail) {
                        throw new SyntaxError(
                            "Undefined block \"{$token->getValue()}\"",
                            $this->getLine(),
                            $this->getTokenStream()->getSource());
                    } 
                    else {
                        return new Node($localNodes, [], $this->getLine(), '');
                    }
                    break;
            }
        }
        return new Node($localNodes, [], $this->getLine(), '');
    }

    public function parseExpression()
    {
        return $this->expressionParser->parse();
    }
        
    public function getTokenStream()
    {
        return $this->stream;
    }

    public function getLine()
    {
        return $this->currentToken()->getLine();
    }

    public function getSource() {
        return $this->stream->getSource();
    }
 
    public function currentToken()
    {
        return $this->stream->currentToken();
    }

    public function isEOF()
    {
        return $this->stream->isEOF();
    }
    
    public function next()
    {
        return $this->stream->next();
    }

    public function expect($type, $values = NULL)
    {
        return $this->stream->expect($type, $values);
    }

    public function test($type, $values = NULL)
    {
        return $this->stream->test($type, $values);
    }
}