<?php

namespace TemplateEngine;

use TemplateEngine\Environment;

abstract class Template 
{
    private $env;
    private $source;
    private $configVariables;

    public function __construct($env, $source) 
    {
        $this->env = $env;
        $this->source = $source;
    }

    protected abstract function displaySelf($context);

    public function display($context) {
        $context = $this->prepareContext($context);
        $this->displaySelf($context);
    }

    protected function displayTemplate($templateRelativePath, $context) 
    {
        $this->env->load($templateRelativePath)->display($context);
    }

    public function render($context) 
    {
        $context = $this->prepareContext($context);
        \ob_start();
        $this->displaySelf($context);
        $renderedText = \ob_get_flush();
        return $renderedText;
    }

    protected function prepareContext($context) {
        $context = \array_merge([
            '__raw_include_dir' => dirname($this->source->getFullPath()),
            '__config_include_dir' => dirname($this->source->getFullPath()),
        ], $context);
        $context = array_merge_recursive($context, [ '__functions' =>  $this->env->getOption('functions')]);
        return $context;
    }

    protected function displayDBVariable($variable, $context) {
        echo $context['db_provider']->get($variable);
    }

    protected function displayRawFile($relativeFilePath, $context) {
        $fullPath = $context['__raw_include_dir'] . DIRECTORY_SEPARATOR . $relativeFilePath;
        $this->env->displayRawFile($fullPath);
    }

    protected function displayConfigVariable($variable, $context) {
        $fullPath = $context['__config_include_dir'] . DIRECTORY_SEPARATOR . basename($this->source->getFullPath()) . '.ini';
        if (!isset($this->configVariables))
            $this->configVariables = $this->env->loadConfigFile($fullPath);
        echo $this->configVariables[$variable];
    }

    public function getSource() {
        return $this->source;
    }
}