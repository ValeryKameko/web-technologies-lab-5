<?php

namespace TemplateEngine;

use TemplateEngine\Node\ICompilable;
use TemplateEngine\Error\UnimlementedError;

class Compiler
{
    private $sourceCode;
    private $startLine;
    private $env;
    private $indentation;

    public function __construct($env)
    {
        $this->env = $env;
    }

    public function compile(ICompilable $node)
    {
        $this->indentation = 0;
        $this->line = 0;
        $this->sourceCode = '';
        $this->startLine = true;
        $this->subcompile($node);

        return $this->sourceCode;
    }

    public function subcompile(ICompilable $node)
    {
        $node->compile($this);
    }

    public function write($string)
    {
        if ($this->startLine) {
            $this->startLine = false;
            for ($i = 0; $i < $this->indentation * 4; $i++)
                $this->sourceCode .= ' ';
        }
        $this->sourceCode .= $string;
    }

    public function writeLine($string) {
        $this->write($string);
        $this->endLine();
    }

    public function getSourceCode() {
        return $this->sourceCode;
    }

    public function endLine()
    {
        $this->sourceCode .= PHP_EOL;
        $this->startLine = true;
    }

    public function indent($count = 1)
    {
        $this->indentation += $count;
    }

    public function outdent($count = 1)
    {
        $this->indentation -= $count;
    }
    
    public function getEnv()
    {
        return $this->env;
    }

    public function writeContextSaving() {
        $this->writeLine("\$context['__parent'] = \$context;");
    }

    public function writeContextRestore() {
        $this->writeLine("\$context = \$context['__parent'];");
    }
}