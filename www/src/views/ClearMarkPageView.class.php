<?php

namespace View;

class ClearMarkPageView extends View {

    public function __construct() {
        parent::__construct();
    }

    public function render($parameters) {
        $template = $this->templateEngineEnvironment->load('clear_mark_page.tmpl');
        $template->render($parameters);
    }
}
