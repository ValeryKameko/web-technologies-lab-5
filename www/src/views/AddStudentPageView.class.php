<?php

namespace View;

class AddStudentPageView extends View {

    public function __construct() {
        parent::__construct();
    }

    public function render($parameters) {
        $template = $this->templateEngineEnvironment->load('add_student_page.tmpl');
        $template->render($parameters);
    }
}
