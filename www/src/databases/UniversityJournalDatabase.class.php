<?php

namespace Database;

class UniversityJournalDatabase extends Database {
    private static $self;

    protected function __construct() {
        parent::__construct('mysql-server', '3306', 'user', 'passwd', 'university-journal');
    }

    public static function getDatabase() {
        if (!isset($self))
            $self = new UniversityJournalDatabase();
        return $self;
    }
}