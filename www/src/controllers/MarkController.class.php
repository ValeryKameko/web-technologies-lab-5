<?php

namespace Controller;

use Database\UniversityJournalDatabase;
use Model\MarkModel;
use Model\StudentModel;
use Model\LessonModel;
use Model\SubjectModel;

class MarkController extends Controller {
    private $database;
    private $studentModel;
    private $lessonModel;
    private $markModel;

    public function __construct() {
        $this->database = UniversityJournalDatabase::getDatabase();
        $this->studentModel = new StudentModel($this->database->getPDO());
        $this->lessonModel = new LessonModel($this->database->getPDO());
        $this->markModel = new MarkModel($this->database->getPDO());
    }
    
    public function handleSetMark($options) {
        $idOptions = array(
            'flags' => FILTER_NULL_ON_FAILURE,
        );

        $markOptions = array(
            'options' => [
                'min_range' => 1,
                'max_range' => 10
            ],
            'flags' => FILTER_NULL_ON_FAILURE,
        );
        
        $studentId = filter_var($options['student_id'] ?? NULL, FILTER_VALIDATE_INT, $idOptions);
        $lessonId = filter_var($options['lesson_id'] ?? NULL, FILTER_VALIDATE_INT, $idOptions);
        $mark = filter_var($options['mark'] ?? NULL, FILTER_VALIDATE_INT, $markOptions);

        if (is_null($studentId) || is_null($lessonId) || is_null($mark))
            Controller::redirect('/set_mark_page?result=error&error=' . urlencode("Not correct query"));
        if (!$this->studentModel->isExists($studentId))
            Controller::redirect('/set_mark_page?result=error&error=' . urlencode("Student doesn't exists"));
        if (!$this->lessonModel->isExists($lessonId))
            Controller::redirect('/set_mark_page?result=error&error=' . urlencode("Lesson doesn't exists"));

        $this->markModel->setMarkValue($lessonId, $studentId, $mark);

        Controller::redirect('set_mark_page?result=ok');
    }

    public function handleClearMark($options) {
        $idOptions = array(
            'flags' => FILTER_NULL_ON_FAILURE,
        );

        $studentId = filter_var($options['student_id'] ?? NULL, FILTER_VALIDATE_INT, $idOptions);
        $lessonId = filter_var($options['lesson_id'] ?? NULL, FILTER_VALIDATE_INT, $idOptions);

        if (is_null($studentId) || is_null($lessonId))
            Controller::redirect('/clear_mark_page?result=error&error=' . urlencode("Not correct query"));
        if (!$this->studentModel->isExists($studentId))
            Controller::redirect('/clear_mark_page?result=error&error=' . urlencode("Student doesn't exists"));
        if (!$this->lessonModel->isExists($lessonId))
            Controller::redirect('/clear_mark_page?result=error&error=' . urlencode("Lesson doesn't exists"));
            
        $this->markModel->clearMarkValue($lessonId, $studentId, $mark);

        Controller::redirect('clear_mark_page?result=ok');
    }

}