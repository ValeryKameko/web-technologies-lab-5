<?php

namespace Controller;

use Database\UniversityJournalDatabase;
use Model\MarkModel;
use Model\StudentModel;
use Model\LessonModel;
use Model\SubjectModel;
use View\AddStudentPageView;

class AddStudentPageController extends Controller {
    private $database;
    private $studentModel;
    private $lessonModel;
    private $subjectModel;
    private $markModel;

    public function __construct() {
        $this->database = UniversityJournalDatabase::getDatabase();
        $this->studentModel = new StudentModel($this->database->getPDO());
        $this->subjectModel = new SubjectModel($this->database->getPDO());
        $this->lessonModel = new LessonModel($this->database->getPDO());
        $this->markModel = new MarkModel($this->database->getPDO());
    }

    public function handle($options) {
        $view = new AddStudentPageView();
        $view->render([
            'result' => $options['result'] ?? NULL,
            'error' => $options['error'] ?? NULL,
        ]);
    }
}