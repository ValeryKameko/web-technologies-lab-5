#!/bin/sh

addgroup mysql mysql

if [ ! -d "/run/mysqld" ]; then
    mkdir -p /run/mysqld
    chown mysql:mysql /run/mysqld
fi

if [ ! -d "/var/lib/mysql/mysql" ]; then
    chown -R mysql:mysql /var/lib/mysql
    mysql_install_db --user=mysql --basedir=/usr --datadir=/var/lib/mysql > /dev/null

    cat << EOF > /root/bootstrap.file
    USE mysql;
    FLUSH PRIVILEGES;
    DELETE FROM mysql.user;
    GRANT ALL ON *.* TO 'root'@'localhost' IDENTIFIED BY '$MYSQL_ROOT_PASSWORD' WITH GRANT OPTION;
    GRANT ALL ON *.* TO '$MYSQL_USER_NAME'@'%' IDENTIFIED BY '$MYSQL_USER_PASSWORD';
    FLUSH PRIVILEGES;
EOF
    mysqld --user=mysql --bootstrap --verbose=0 --skip-networking=0 < /root/bootstrap.file > /dev/null
fi

for file in /usr/bin/db_install/*.sql; do
    mysqld --user=mysql --bootstrap --verbose=0 --skip-networking=0 < $file > /dev/null
done

mysqld --user=mysql --basedir=/usr --datadir=/var/lib/mysql > /dev/null