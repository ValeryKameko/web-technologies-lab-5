CREATE DATABASE IF NOT EXISTS `university-journal` COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `university-journal`.`student` (
    `id` int NOT NULL AUTO_INCREMENT,
    `name` varchar(256) NOT NULL,
    PRIMARY KEY (`id`)
) DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `university-journal`.`subject` (
    `id` int NOT NULL AUTO_INCREMENT,
    `name` varchar(256) NOT NULL UNIQUE,
    PRIMARY KEY (`id`)
) DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `university-journal`.`lesson` (
    `id` int NOT NULL AUTO_INCREMENT,
    `date` date NOT NULL,
    `subject_id` int NOT NULL,
    PRIMARY KEY (`id`),
    FOREIGN KEY (`subject_id`)
	    REFERENCES `subject`(`id`)
        ON DELETE CASCADE
) DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


CREATE TABLE IF NOT EXISTS `university-journal`.`mark` (
    `value` int NOT NULL,
    `lesson_id` int NOT NULL,
    `student_id` int NOT NULL,
    PRIMARY KEY (`lesson_id`, `student_id`),
    FOREIGN KEY (`lesson_id`)
	    REFERENCES `lesson`(`id`)
        ON DELETE CASCADE,
    FOREIGN KEY (`student_id`)
	    REFERENCES `student`(`id`)
        ON DELETE CASCADE
) DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
